import React from 'react'
import { Outlet } from 'react-router-dom'
import Footer from '../components/Footer'
import Navigation from '../components/Navigation'
import PopupChat from '../components/PopupChat'
import "../styles/home.scss"

const Home = () => {
  
  return (
    <div className='home-wrapper'>
      <Navigation /> 
      <Outlet>
        
      </Outlet>
      
        <PopupChat />
        <Footer />
      
    </div>
  )
}

export default Home