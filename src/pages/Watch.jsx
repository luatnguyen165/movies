import React from 'react'
import Recommend from '../components/Recommend';
import Player from '../components/Player';
import"../styles/watch.scss"


const Watch = () => {
  return (
    <div className='watch-wrapper'>

      <div className="watch-content">
        <div className='watch-left'>
          <Player />
        </div> 

        <div className="watch-right">
            <Recommend />
        </div>
         
      </div>

    </div>
  )
}

export default Watch