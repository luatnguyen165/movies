import React from 'react'
import "../styles/detail.scss"

import List from "../components/List"


const Detail = () => {
  return (
    <div className='detail-wrapper'>

        <div className="detail-content">
          <List />
        </div>

    </div>
  )
}

export default Detail