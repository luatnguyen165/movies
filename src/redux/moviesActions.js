import {
    FETCH_MOVIES,
    FETCH_MOVIES_FAILED,
    FETCH_MOVIES_SUCCESS,
}   from "./actionType"
import { fetchMoviesService } from "../service/index"

const moviesAction = {
    fetchingMovies: () => ({
        type: FETCH_MOVIES
    }),

    fetchMoviesSuccess: movies => ({
        type: FETCH_MOVIES_SUCCESS,
        payload: movies
    }),

    fetchMoviesFailed: error => ({
        type: FETCH_MOVIES_FAILED,
        payload: error
    })
}

export const loadMovies = () => dispatch =>{
    dispatch( moviesAction.fetchingMovies() );
    
    fetchMoviesService.fetchMovies()
        .then(movies => dispatch( moviesAction.fetchMoviesSuccess(movies.data)) )
        .catch(error => dispatch( moviesAction.fetchMoviesFailed(error)) )
}
