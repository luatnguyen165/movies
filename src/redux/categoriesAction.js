import{
    FETCH_CATEGORIES,
    FETCH_CATEGORIES_FAILED,
    FETCH_CATEGORIES_SUCCESS
} from "./actionType"
import { fetchCategoriesService } from "../service/index"

const categoriesAction = {
    fetchingCategories: () => ({
        type: FETCH_CATEGORIES
    })
    ,
    fetchCategoriesSuccess: categories => ({
        type: FETCH_CATEGORIES_SUCCESS,
        payload: categories
    }),
    fetchCategoriesFailed: error => ({
        type: FETCH_CATEGORIES_FAILED,
        payload: error
    })
}

const loadCategories = () => dispatch =>{
    dispatch( categoriesAction.fetchingCategories() );
    fetchCategoriesService.fetchCategories()
        .then(categories => dispatch( categoriesAction.fetchCategoriesSuccess(categories.data)) )
        .catch(error => dispatch( categoriesAction.fetchCategoriesFailed(error)) )
}

export default loadCategories