export const FETCH_MOVIES = "movie-loading";
export const FETCH_MOVIES_FAILED = "movie-failed";
export const FETCH_MOVIES_SUCCESS = "movie-success";

export const FETCH_CATEGORIES = "catagories-loading";
export const FETCH_CATEGORIES_FAILED = "catagories-failed";
export const FETCH_CATEGORIES_SUCCESS = "catagories-success";