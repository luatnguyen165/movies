import { legacy_createStore, applyMiddleware } from "redux"
import rootReducer from "./root-reducer"
import reduxThunk from "redux-thunk"
import logger from "redux-logger"

const middlewares = [reduxThunk];

if (process.env.NODE_ENV ==="development"){
    middlewares.push(logger);
}

const store = legacy_createStore(rootReducer, applyMiddleware(...middlewares));

export default store;
