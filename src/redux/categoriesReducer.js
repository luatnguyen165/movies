import{
    FETCH_CATEGORIES,
    FETCH_CATEGORIES_FAILED,
    FETCH_CATEGORIES_SUCCESS
} from "./actionType"

const initialState = {
    loading: false,
    categories: null,
    error: null
}

const categoriesReducer = (state = initialState, action)=>{
    switch(action.type){
        case FETCH_CATEGORIES:
            return {
                loading: true,
                categories: null,
                error: null
            }
        case FETCH_CATEGORIES_SUCCESS:
            return {
                loading: false,
                categories: action.payload,
                error: null
            }
        case FETCH_CATEGORIES_FAILED:
            return {
                loading: false,
                categories: null,
                error: action.payload
            }
        default:
            return state
    }
}

export default categoriesReducer;
