import { combineReducers } from "redux";
import moviesReducer from "./moviesReducer";
import categoriesReducer from "./categoriesReducer";

const reducer = combineReducers({ moviesReducer, categoriesReducer });

export default reducer;