import {
    FETCH_MOVIES,
    FETCH_MOVIES_FAILED,
    FETCH_MOVIES_SUCCESS,
}   from "./actionType"

const initialState = {
    loading: false,
    movies: null,
    error: null
}

const moviesReducer = (state = initialState, action)=>{
    switch(action.type){
        case FETCH_MOVIES:
            return {
                loading: true,
                movies: null,
                error: null
            }
        case FETCH_MOVIES_SUCCESS:
            return {
                loading: false,
                movies: action.payload,
                error: null
            }
        case FETCH_MOVIES_FAILED:
            return {
                loading: false,
                movies: null,
                error: action.payload
            }
        default:
            return state
    }
}

export default moviesReducer;

