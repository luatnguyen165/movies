import axios from "axios";

export const fetchMoviesService ={
    fetchMovies:()=>{
        return new Promise (( resolve, reject ) => {
            axios.get(`${process.env.REACT_APP_urlAPI}/movies`)
            .then(response =>resolve( response.data ))
            .catch(error => reject(error))
        })
    }
}

export const fetchCategoriesService ={
    fetchCategories:()=>{
        return new Promise (( resolve, reject ) => {
            axios.get(`${process.env.REACT_APP_urlAPI}/categories/list-category`)
            .then(response => resolve( response.data ))
            .catch(error => reject(error))
        })
    },
}