import React from 'react'
import "../styles/list.scss"
import Movie from "./Movie"
import Pagination from '@mui/material/Pagination';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    white: {
      main: '#7a7a7a',
    }
  }
});

const List = () => {
  return (
    <div className='list-wrapper'>
        <div className="list-navigate">
            <h2>PHIM MỚI</h2>

            <div className="list-search">

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Thể loại</option>
                    <option value="">Phim hành động</option>
                    <option value="">Phim hành động</option>
                </select>

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Quốc gia</option>
                    <option value="">Phim hành động</option>
                </select>

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Năm phát hành</option>
                    <option value="">Phim hành động</option>
                </select>

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Ngôn ngữ</option>
                    <option value="">Phim hành động</option>
                </select>

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Sắp xếp</option>
                    <option value="">Phim hành động</option>
                </select>

                <select name="Thể loại" id="" className='list-search-select'>
                    <option value="" >Hình thức</option>
                    <option value="">Phim hành động</option>
                </select>

                <span className='list-search-button'>
                    Tìm kiếm
                </span>
            </div>
        </div>
        
        <div className="list-content">
            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>

            <div className="list-item">
                <Movie />
            </div>
            <div className="list-pagination">
                <ThemeProvider theme={theme}>
                    <Pagination count={10} shape="rounded"  color='white'/>
                </ThemeProvider>
            </div>
            
        </div>

    </div>
  )
}

export default List