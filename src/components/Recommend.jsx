import React from 'react'
import "../styles/recommend.scss"
import Movie from './Movie'

const Recommend = () => {
  return (
    <div className='recommend-wrapper'>
        <h2>PHIM MỚI</h2>
        <div className="recommend-content">
            <div className="recommend-item">
                <Movie />
                <hr />    
            </div>
            
            <div className="recommend-item">
                <Movie />
                <hr />    
            </div>
            
            <div className="recommend-item">
                <Movie />
                <hr />    
            </div>
            
            <div className="recommend-item">
                <Movie />
                <hr />    
            </div>
            
            <div className="recommend-item">
                <Movie />
            </div>        
        </div>

    </div>
  )
}

export default Recommend