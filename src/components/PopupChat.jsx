import React from 'react'
import socketIOClient from "socket.io-client";
import { useState, useEffect } from 'react';
import "../styles/popup.scss"
import ChatIcon from '@mui/icons-material/Chat';
import SmsIcon from '@mui/icons-material/Sms';
import PersonIcon from '@mui/icons-material/Person';
import CloseIcon from '@mui/icons-material/Close';
import SendIcon from '@mui/icons-material/Send';
import { useForm } from "react-hook-form";

const ENDPOINT = "http://127.0.0.1:5000";

const PopupChat = () => {
  const { register, handleSubmit, setFocus ,setValue } = useForm();
  const [close, setClose]= useState(false);
  const [listMsg, setListMsg] = useState([]) 
  

  const handleOpenPopup = () => {
    setClose(true);
  }

  const onSubmit = (data,) => {
    const socket = socketIOClient(ENDPOINT);
    socket.emit("msgToServer", data)
    setValue("text", "");
    setFocus("text");
  }



  useEffect(()=>{
    if(close){
      document.getElementById("chat-content").scrollTo(0, document.getElementById("chat-content").scrollHeight)
    }
  },[close])

  useEffect(() => {
    const socket = socketIOClient(ENDPOINT);
    console.log('socket',socket)
    
    socket.on("msgToClient", message => {
      console.log('data',message)
      setListMsg([message, ...listMsg])
    });
    return ()=> socket.disconnect()
  }, [listMsg])
  

  return (
    <div className='chat-wrapper' >
      <div className="chat-open" onClick={()=>handleOpenPopup()} >
        <ChatIcon className='chat-icon-open' style={close? {display: "none"} : null}/>
      </div>
      

      {close&&
        <div className="chat-window" >
          <div className="chat-icon-close" onClick={()=>setClose(false)}>
            <CloseIcon className='chat-window-close'/>
          </div>
            

          <h4 id="g34">CHAT NGAY</h4>

          <div className="chat-content" id='chat-content' >
            <ul>
             {
             listMsg.map((item, index) =>(
              <li key={`${item.name}-${index}`}>
                <h5>{`${item.name}:`}</h5>
                <span>{item.text}</span>
                </li>
             ))}
                
            </ul>
          </div>

          <form className="chat-form" onSubmit={handleSubmit(onSubmit)} >
            <div className="chat-form-item chat-form-name">
              <label htmlFor=""><PersonIcon /></label>
              <input type="text" id="name" placeholder='Nhập tên' {...register("name", { required: true })}/>
            </div>
            
            <div className="chat-form-item chat-form-msg" >
              <label htmlFor="msg" ><SmsIcon /></label>
              <input  type="text"  id='msg' placeholder='Nhập tin nhắn' {...register("text", { required: true })}/>
              <button type="submit" ><SendIcon className='chat-form-submit'/></button>
            </div>
          </form>
        </div>
      }
    </div>

  )
}

export default PopupChat