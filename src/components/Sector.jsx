import React from 'react'
import "../styles/sector.scss"
import Movie from './Movie'
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { Link } from 'react-router-dom';

const Sector = () => {

return (
    <div className='sector-wrapper'>
      <div className="sector-nav">
        <h2>PHIM LẺ MỚI CẬP NHẬT</h2>

        <Link to="/detail">
          <div className='sector-button-detail'>
            <input type="submit" value="Xem tất cả" />
            <ArrowRightIcon />
          </div>
        </Link>
        
      </div>

      <div className="sector-grid">
        <div className="sector-grid-1">
        <Movie />
        </div>
        
        <div className="sector-grid-2">
          <Movie className="sector-grid-2"/>
        </div>

        <div className="sector-grid-3">
        <Movie />
        </div>

        <div className="sector-grid-4">
        <Movie />
        </div>

        <div className="sector-grid-5">
        <Movie />
        </div>

        <div className="sector-grid-6">
        <Movie />
        </div>

        <div className="sector-grid-7">
        <Movie />
        </div>

        <div className="sector-grid-8">
        <Movie />
        </div>

        <div className="sector-grid-9">
        <Movie />
        </div>

        <div className="sector-grid-10">
        <Movie />
        </div>

        <div className="sector-grid-11">
        <Movie />
        </div>

        <div className="sector-grid-12">
        <Movie />
        </div>
      </div>
    </div>
  )
}

export default Sector