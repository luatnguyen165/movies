import React from 'react'
import "../styles/player.scss"
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownAltIcon from '@mui/icons-material/ThumbDownAlt';
import FavoriteIcon from '@mui/icons-material/Favorite';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import { FacebookProvider, Comments } from 'react-facebook';

const Player = () => {
  return (
    <div className='player-wrapper'>
        <h2>MONEY HEIST: KOREA</h2>

        <iframe src="https://drive.google.com/file/d/1ul5LmorLnVJ2bb7PBQ3k-qLyeJWsL3E_/preview" width="100%" height="590px" allow="autoplay" allowFullScreen title='video'></iframe>

        <div className="player-interact">
            <div className="player-switch-server">

                <div className="player-server-title">
                    <p>Đổi Server:</p>
                </div>

                <div className="player-server player-server-active">
                    <p>Server 1</p>
                </div>

                <div className="player-server">
                    <p>Server 2</p>
                </div>

                <div className="player-server">
                    <p>Server 3</p>
                </div>
            </div>

            <div className="player-fame">
                <div className="player-fame-btn">
                    <p>19.123</p>
                    <ThumbUpIcon />
                </div>

                <div className="player-fame-btn">
                    <p>19.123</p>
                    <ThumbDownAltIcon />
                </div>

                <div className="player-fame-btn">
                    <p>19.123</p>
                    <FavoriteIcon />
                </div>

                <div className="player-fame-btn">
                    <p>19.123</p>
                    <RemoveRedEyeIcon />
                </div>

            </div>
        </div>

        <div className="player-espisodes">
            <span>Tập phim:</span>
            <ul>
                <li className='player-espisode-active'>Tập 1</li>
                <li>Tập 2</li>
                <li>Tập 3</li>
            </ul>
        </div>

        <div className="player-info">
            <Typography component="legend" fontWeight={700}>Movie Rating</Typography>

            <Rating
                name="half-rating"
                defaultValue={6.5}
                precision= {0.5}
                max= {10}
                size= "large"
                
            />

            <div className="player-desc">
                <ul>
                    <li>
                        <h6>Đang phát:
                            <span> HD Vietsub</span>
                        </h6>
                    </li>
                    
                    <li>
                        <h6>Năm Phát Hành:
                            <span> 2022</span>
                        </h6>
                    </li>

                    <li>
                        <h6>Quốc gia:
                            <span> Phim Trung Quốc</span>
                        </h6>
                    </li>

                    <li>
                        <h6>Thể loại: 
                            <span> Phim Hành Động</span>,
                            <span> Phim Võ Thuật</span>
                        </h6>
                    </li>

                    <li>
                        <h6>Đạo diễn: 
                            <span> Lâm Nam</span>
                        </h6>
                    </li>

                    <li>
                        <h6>Thời lượng: 
                            <span> 45 phút/tập</span>
                        </h6>
                    </li>

                    <li>
                        <h6>Diễn viên: 
                            <span> Hứa Khải, Chung Sở Hi, Wayne, Bao Bei Er, Ruan Ju.</span>
                        </h6>
                    </li>
                </ul>
                <hr />

            </div>

            <div className="player-content">
                <Typography component="legend" fontWeight={700}>Nội dung phim</Typography>

                <span>Lạc Lối Dưới Lòng Côn Luân Lost In The KunLun Mountains 2022 Full HD Vietsub Thuyết 
                    Minh Vào thời Trung Hoa Dân Quốc, chàng trai tài giỏi và tốt bụng Ding Yun Qi đang trong
                     quá trình điều tra vụ mất tích kỳ lạ của cha mình đã vô tình phát hiện ra bí mật của Di tích Côn Lôn.</span>
                <hr />
            </div>

            <div className="player-comment" >
                <Typography component="legend" fontWeight={700} >Bình luận</Typography>
                <FacebookProvider appId="817128926404794" >
                    <Comments href="http://localhost:3001/watch" width="100%"/>
                </FacebookProvider>
            </div>
            
        </div>
    </div>
  )
}

export default Player