import React from 'react'
import Movie from "./Movie"

import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';

import "swiper/css"
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import "../styles/slider.scss"

const Slider = () => {
  return (
    <div className='slider-wrapper'>
      <h2>PHIM ĐỀ CỬ</h2>
      <Swiper 
      navigation
      modules={[Navigation, Autoplay]} 
      className="mySwiper"
      spaceBetween= {9}
      slidesPerView={5}
      slidesPerGroup={5}
      loopFillGroupWithBlank={true}
      loop= {true}
      autoplay= {{
        delay: 2000,
        pauseOnMouseEnter: true,
        disableOnInteraction: false
      }}
      >
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        <SwiperSlide className="slider-item"><Movie /></SwiperSlide>
        
      </Swiper>
    </div>
  )
}

export default Slider