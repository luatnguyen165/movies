import React from 'react'
import "../styles/navigation.scss"
import SearchIcon from '@mui/icons-material/Search';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import CloseIcon from '@mui/icons-material/Close';

const Navigation = () => {
    const [exit, setExit]= useState(false);
    const [register, setRegister]= useState(false);
  return (
    <div className='navigation-wrapper'>
        <div className="navigation-items">
            <div className="navigation-logo">
                <Link  to="/"><img src="https://via.placeholder.com/160x50" alt="" /></Link>
            </div>

            <div className="navigation-item">
               <Link to="/"><h5>PHIM MOI</h5></Link>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>PHIM LẺ</h5></Link>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>PHIM BỘ</h5></Link>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>THỂ LOẠI</h5></Link>
                <ul>
                    <Link to="/detail"><li>Phim Hành Động</li></Link>
                    <Link to="/detail"><li>Phim Tình Cảm</li></Link>
                    <Link to="/detail"><li>Phim Hài Hước</li></Link>
                    <Link to="/detail"><li>Phim Cổ Trang</li></Link>
                    <Link to="/detail"><li>Phim Tâm Lý</li></Link>
                    <Link to="/detail"><li>Phim Hình Sự</li></Link>
                    <Link to="/detail"><li>Phim Chiến Tranh</li></Link>
                </ul>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>QUỐC GIA</h5></Link>
                <ul>
                    <Link to="/detail"><li>Phim Trung Quốc</li></Link>
                    <Link to="/detail"><li>Phim Nhật Bản</li></Link>
                    <Link to="/detail"><li>Phim Việt Nam</li></Link>
                    <Link to="/detail"><li>Phim Mỹ</li></Link>
                    <Link to="/detail"><li>Phim Ấn Độ</li></Link>
                    <Link to="/detail"><li>Phim Nhật</li></Link>
                </ul>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>NĂM PHÁT HÀNH</h5></Link>
                <ul>
                    <Link to="/detail"><li>Năm 2022</li></Link>
                    <Link to="/detail"><li>Năm 2021</li></Link>
                    <Link to="/detail"><li>Năm 2020</li></Link>
                    <Link to="/detail"><li>Năm 2019</li></Link>
                    <Link to="/detail"><li>Năm 2018</li></Link>
                    <Link to="/detail"><li>Năm 2017</li></Link>
                    <Link to="/detail"><li>Năm 2016</li></Link>
                    <Link to="/detail"><li>Năm 2015</li></Link>
                </ul>
            </div>

            <div className="navigation-item">
                <Link to="/detail"><h5>PHIM CHIẾU RẠP</h5></Link>
            </div>

            <div className="navigation-item ">
                <Link to="/detail"><h5 className= "last-item">TOP PHIM</h5></Link>
                <ul>
                    <Link to="/detail"><li>Top IMDB</li></Link>
                    <Link to="/detail"><li>Phim Netflix</li></Link>
                    <Link to="/detail"><li>Phim Marvel</li></Link>
                    <Link to="/detail"><li>Phim Hot</li></Link>
                    <Link to="/detail"><li>Phim DC comic</li></Link>
                    <Link to="/detail"><li>Phim HD</li></Link>
                </ul>
            </div>
        </div>

        <div className="navigation-options">
            <div className="navigation-search">
                <input type="text" placeholder='Tìm phim...' />
                <SearchIcon/>
            </div>

            <div className="navigation-login" onClick={()=>{setExit(true)}}>
                    <p>Đăng nhập</p>
            </div>
            
        </div>

        {exit&&
        <div>
            <div className="navigation-login-wrapper" >
                <div className="navigation-login-background" 
                onClick={()=>{
                    setExit(false);
                    setRegister(false);
                }}
                
                >
                    <CloseIcon className='navigation-login-close-icon'/>
                </div>

                <div className="navigation-login-form">
                {!register?
                    <form action="" id="ádas" name="ádasd">
                        <label htmlFor="account">Tên tài khoản</label>
                        <input type="text" id="account"/>

                        <label htmlFor="password">Mật khẩu</label>
                        <input type="password" id="password"/>

                        <input type="submit" value="Đăng nhập"/>

                        <button onClick={()=>{setRegister(true)}}>Đăng kí</button>
                    </form>
                    :
                    <form action="" id="ádas" >
                        <label htmlFor="account">Tên Tài khoản</label>
                        <input type="text" id="account"/>

                        <label htmlFor="password">Mật khẩu</label>
                        <input type="password" id="password"/>

                        <label htmlFor="pre-password">Nhập lại Mật khẩu</label>
                        <input type="text" id="pre-password" />
                        
                        <input type="submit" value="Xác nhận"/>
                    </form>

                }
                </div>
            </div>
        </div>
        }
    </div>
    )
}
export default Navigation