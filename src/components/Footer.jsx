import React from 'react'
import "../styles/footer.scss"
import { Link } from "react-router-dom"
const Footer = () => {
  return (
    <div className='footer-wrapper'>
        <div className="footer-logo">
            <Link  to="/"><img src="https://via.placeholder.com/160x50" alt="" /></Link>
        </div>

        <div>
            <p>Phim Mới</p>
            <ul>
                <Link to="/detail"><li>Phim Chiếu Rạp</li></Link>
                <Link to="/detail"><li>Phim Lẻ</li></Link>
                <Link to="/detail"><li>Phim Bộ</li></Link>
                <Link to="/detail"><li>Phim Hành Động</li></Link>
                <Link to="/detail"><li>Phim Viễn Thông</li></Link>
                <Link to="/detail"><li>Phim Tâm Lý</li></Link>
                <Link to="/detail"><li>Phim Hài Hước</li></Link>
            </ul>
        </div>

        <div>
            <p>Phim Hay</p>
            <ul>
                <Link to="/detail"><li>Phim Mỹ</li></Link>
                <Link to="/detail"><li>Phim Hàn Quốc</li></Link>
                <Link to="/detail"><li>Phim Trung Quốc</li></Link>
                <Link to="/detail"><li>Phim Việt Nam</li></Link>
                <Link to="/detail"><li>Phim Thái Lan</li></Link>
                <Link to="/detail"><li>Phim Ma Kinh Dị</li></Link>
                <Link to="/detail"><li>Phim Hoạt Hình</li></Link>
            </ul>
        </div>

        <div>
            <p>Phim Hot</p>
            <ul>
                <li>Phimmoi</li>
                <li>Sitemap</li>
            </ul>
        </div>

        <div>
            <p>Trợ giúp</p>
            <ul>
                <li>Hỏi đáp</li>
                <li>Liên hệ</li>
                <li>Tin tức</li>
            </ul>
        </div>

        <div>
            <p>Điều khoản sử dụng</p>
            <ul>
                <li>Chính sách riêng tư</li>
                <li>Khiếu nại bản quyền</li>
            </ul>
            <p>© 2021 PhimChill.Net</p>
        </div>

        

    </div>
  )
}

export default Footer