import React from 'react'
import "../styles/main.scss"
import Sector from './Sector'
import Slider from './Slider'

const Main = () => {
  return (
    <div>
        <div  className='main-content'>
            <Slider />
            <Sector />
            <Sector />
        </div>
    </div>
  )
}

export default Main