import React from 'react'
import "../styles/movie.scss"
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import PlayArrowRoundedIcon from '@mui/icons-material/PlayArrowRounded';
import { Link } from 'react-router-dom';

const Movie = () => {
  return (
    <Link to="/watch">
    <div className='movie-wrapper'>
      <span>
        <p>HD Vietsub</p>
        <ArrowDropDownIcon />
      </span>

      <img  src="https://bloganchoi.com/wp-content/uploads/2022/06/phim-han-quoc-thang-6-2022-1.jpg" alt="poster flim" />
      
        <div className='movie-icon'>
          <PlayArrowRoundedIcon className='movie-play-icon'/>
        </div>
        
      <h5>Money Heist: KOREA</h5>
      
    </div>
    </Link>
  )
}

export default Movie