import React from 'react'
import { Row, Breadcrumb } from 'antd';
import { Outlet, useLocation, Link } from "react-router-dom"

const User = () => {
  const location = useLocation();
  const breadtitle= location.pathname.split("/")

  return (
    <>
      <Row style={{background: "#fff", padding: "12px", borderRadius: "5px"}}>
          <Breadcrumb>
            <Breadcrumb.Item>
            <Link to={""}>{breadtitle[2].charAt(0).toUpperCase() + breadtitle[2].slice(1)}</Link>
            </Breadcrumb.Item>
            
            {breadtitle[3]&&
              <Breadcrumb.Item>
                <Link to={breadtitle[3]}>{breadtitle[3].charAt(0).toUpperCase() + breadtitle[3].slice(1)}</Link>
              </Breadcrumb.Item>
            }
          </Breadcrumb>
      </Row>
      <Row style={{ borderRadius: "5px", margin: "24px 0"}}>
        <Outlet context={[breadtitle[2]]}/>
      </Row>
    </>
  )
}

export default User