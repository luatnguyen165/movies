import React from 'react'
import Cards from '../components/Cards'
import OverallDisplay from '../components/OverallDisplay'
import { Row } from 'antd';
import "../styles/dashboard.scss"

const Dashboard = () => {
  return (
    <>
      <Cards />
        <Row  style={{width: "100%", height: "412px", background: "#fff", marginTop: "24px", borderRadius: "5px", padding: "0 20px"}}>
          <OverallDisplay />
        </Row>
    </>
  )
}

export default Dashboard