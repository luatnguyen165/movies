import React, { useState } from 'react'
import { Outlet, useNavigate} from "react-router-dom"
import { Layout, Col, Row, Avatar, Input, Badge, Menu } from 'antd';
import { SearchOutlined, BellOutlined, UserOutlined } from '@ant-design/icons';
import CategoryOutlinedIcon from '@mui/icons-material/CategoryOutlined';
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';
import LocalMoviesOutlinedIcon from '@mui/icons-material/LocalMoviesOutlined';
import MovieOutlinedIcon from '@mui/icons-material/MovieOutlined';
import "antd/dist/antd.min.css"
import "../styles/admin.scss"

const Admin = () => {
  const { Header, Sider, Content } = Layout;
  const { Search } = Input;
  const navigate = useNavigate()
  return (
    <div className='admin-wrapper'>
        <Layout>
          <Header className='admin-header' >
            <Row justify="space-between" align='middle' className='admin-header-navigate'>
              <Col >
                <img src="https://via.placeholder.com/160x50" alt="" />
              </Col>

              <Row gutter={[16, 0]}> 

                <Col >
                  <Search placeholder="Searching..." enterButton= {<SearchOutlined style={{fontSize: '25px'}}/>} />
                </Col>

                <Col >
                  <Badge count="1">
                    <BellOutlined style={{fontSize: "25px", color: "white"}}/>
                  </Badge>
                </Col>

                <Col >
                  <Avatar size= "large" src="https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes.png"/>
                </Col>
              </Row>
              
            </Row>
            </Header>

          <Layout>
            <Sider theme='light' style= {{ minHeight: "91vh" }} collapsible collapsedWidth={50} defaultCollapsed={true}>

            <Menu
                theme="light"
                mode="inline"
                defaultSelectedKeys={['']}
                onSelect= {({ key })=>navigate(key)}
                items={[
                  {
                    key: '',
                    icon: <DashboardOutlinedIcon />,
                    label: 'Dashboard',
                  },
                  {
                    key: 'category',
                    icon: <CategoryOutlinedIcon />,
                    label: 'Category',
                  },
                  {
                    key: 'movie',
                    icon: <LocalMoviesOutlinedIcon />,
                    label: 'Movie',
                  },
                  {
                    key: 'episode',
                    icon: <MovieOutlinedIcon />,
                    label: 'Episode',
                  },
                  {
                    key: 'user',
                    icon: <UserOutlined />,
                    label: 'User',
                  },
                ]}
              />

            </Sider>

            <Content
              className='admin-content'
              style={{
                padding:" 20px 24px"
              }}
            >
              <Outlet />

              </Content>
          </Layout>
          
        </Layout>
    </div>
  )
}

export default Admin