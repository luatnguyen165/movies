import React from 'react'
import { Col, Row, Card, Statistic, Typography } from 'antd';
import { ArrowUpOutlined, ArrowDownOutlined } from '@ant-design/icons';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import "../styles/card.scss"
const Cards = () => {
    const { Text } = Typography;
  return (
    <div className='card-wrapper'>
        <Row gutter={32}>

            <Col className="gutter-row" span={6} >
                <Card 
                title="Total sales" 
                extra= {<ErrorOutlineOutlinedIcon />}
                bodyStyle= {{padding: "0 24px"}}
                style={{borderRadius: "5px"}}
                >

                <Text italic style={{fontSize: "2rem"}}>1235</Text>
                
                <Row style={{ height: "46px"}} gutter={8} align="middle">
                    <Col span={12} >
                        <Statistic
                        title="Weekly"
                        value={11.28}
                        precision={2}
                        valueStyle={{ color: '#f5222d', fontSize: "0.9rem", marginLeft: "5px"}}
                        prefix={<ArrowDownOutlined />}
                        suffix="%"
                        />
                        </Col>

                    <Col span={12}>
                        <Statistic
                        title="Yoy"
                        value={11.28}
                        precision={2}
                        valueStyle={{ color: '#3f8600', fontSize: "0.9rem", marginLeft: "5px" }}
                        prefix={<ArrowUpOutlined />}
                        suffix="%"
                        />
                    </Col>
                </Row>
                <hr />

                <Row>
                    <Statistic
                        title="Daily sales"
                        value={"￥12,423"}
                        precision={2}
                        valueStyle={{ color: '#000000', fontSize: "0.9rem", marginLeft: "5px"}}

                    />
                </Row>
                
                </Card>
            </Col>

            <Col className="gutter-row" span={6}>
                <Card 
                    title="Views" 
                    extra= {<ErrorOutlineOutlinedIcon />} 
                    bodyStyle= {{padding: "0 24px"}} 
                    style={{borderRadius: "5px"}}
                >
                    <Text italic style={{fontSize: "2rem"}}>1235</Text>
                        <Row style={{ height: "46px"}} gutter={8} align="middle">
                        <Col span={12} >
                            <Statistic
                            title="Weekly"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#f5222d', fontSize: "0.9rem", marginLeft: "5px"}}
                            prefix={<ArrowDownOutlined />}
                            suffix="%"
                            />
                            </Col>

                        <Col span={12}>
                            <Statistic
                            title="Yoy"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#3f8600', fontSize: "0.9rem", marginLeft: "5px" }}
                            prefix={<ArrowUpOutlined />}
                            suffix="%"
                            />
                        </Col>
                        </Row>
                        <hr />

                        <Row>
                            <Statistic
                                title="Daily sales"
                                value={"￥12,423"}
                                precision={2}
                                valueStyle={{ color: '#000000', fontSize: "0.9rem", marginLeft: "5px"}}

                            />
                        </Row>
                </Card>
            </Col>

            <Col className="gutter-row" span={6}>
                <Card 
                title="number of payments" 
                extra= {<ErrorOutlineOutlinedIcon />} 
                bodyStyle= {{padding: "0 24px"}}
                style={{borderRadius: "5px"}}
                >
                    <Text italic style={{fontSize: "2rem"}}>1235</Text>
                    <Row style={{ height: "46px"}} gutter={8} align="middle">
                        <Col span={12} >
                            <Statistic
                            title="Weekly"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#f5222d', fontSize: "0.9rem", marginLeft: "5px"}}
                            prefix={<ArrowDownOutlined />}
                            suffix="%"
                            />
                        </Col>

                        <Col span={12}>
                            <Statistic
                            title="Yoy"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#3f8600', fontSize: "0.9rem", marginLeft: "5px" }}
                            prefix={<ArrowUpOutlined />}
                            suffix="%"
                            />
                        </Col>
                    </Row>
                    <hr />

                    <Row>
                        <Statistic
                            title="Daily sales"
                            value={"￥12,423"}
                            precision={2}
                            valueStyle={{ color: '#000000', fontSize: "0.9rem", marginLeft: "5px"}}

                        />
                    </Row>

                </Card>
            </Col>

            <Col className="gutter-row" span={6}>
                <Card 
                    title="process" 
                    extra= {<ErrorOutlineOutlinedIcon />} 
                    bodyStyle= {{padding: "0 24px"}}
                    style={{borderRadius: "5px"}}
                >
                <Text italic style={{fontSize: "2rem"}}>1235</Text>
                    <Row style={{ height: "46px"}} gutter={8} align="middle">
                        <Col span={12} >
                            <Statistic
                            title="Weekly"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#f5222d', fontSize: "0.9rem", marginLeft: "5px"}}
                            prefix={<ArrowDownOutlined />}
                            suffix="%"
                            />
                        </Col>

                        <Col span={12}>
                            <Statistic
                            title="Yoy"
                            value={11.28}
                            precision={2}
                            valueStyle={{ color: '#3f8600', fontSize: "0.9rem", marginLeft: "5px" }}
                            prefix={<ArrowUpOutlined />}
                            suffix="%"
                            />
                        </Col>
                    </Row>
                    <hr />

                    <Row>
                        <Statistic
                            title="Daily sales"
                            value={"￥12,423"}
                            precision={2}
                            valueStyle={{ color: '#000000', fontSize: "0.9rem", marginLeft: "5px"}}

                        />
                    </Row>

                </Card>
            </Col>
        </Row>
    </div>
  )
}

export default Cards