import React from 'react'
import "../styles/new.scss"
import { Row, Divider, Form, Button, Input, Col } from 'antd';
import { useOutletContext } from 'react-router-dom';

const New = () => {
  const [pathname]= useOutletContext();

  return (
    <>
      <Row style={{background: "#fff", width: "100%", borderRadius: "5px"}}>
        <Divider style={{margin: "0", fontSize:"2rem"}} orientation="left">{`Add New ${pathname.charAt(0).toUpperCase() + pathname.slice(1)}`}</Divider>
      </Row>


        <Row style={{width: "100%", background: "#fff", marginTop: "24px", padding: "24px"}}>
          <Form
            layout= "inline"
          >   
              <Row gutter={[16,16]} span={24} >
              {
                TITTLE_INPUT[pathname].map(item=>(
                  <Col span={12}>
                    <Form.Item 
                      label= {item[0]}
                      name= {item[1]}
                      rules={[{ required: true, message: 'Title is required!' }]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                ))
              } 
              <Col span={16}>
                  <Form.Item
                    label="Avatar"
                    name="avatar"
                    rules={[{ required: true, message: 'Title is required!' }]}
                    >
                    <input type="file" />
                  </Form.Item>
                </Col>
              </Row>
                
              <Row style={{marginLeft: "auto"}}>
                <Form.Item label="">
                  <Button htmlType='submit' type='primary'>Post</Button>
                </ Form.Item>
              </Row>

          </Form>

        </Row>

    </>
  )
}

export default New;

const TITTLE_INPUT={
  category: [
    ["Title", "title"],
    ["Description", "description"]
  ],

  movie: [
    ["Title", "title"],
    ["Description", "description"],
    ["Status", "status"],
    ["National", "national"],
    ["Category Belong to", "cate"],
    ["Evaluate", "evalute"],
    ["Type", "type"],
    ["Director", "director"],
    ["Cast", "cast"],
    ["Duration", "time_movie"],
    ["Year", "year"]
  ],

  episode: [
    ["Title", "episode_title"],
    ["Server 1", "server_one"],
    ["Server 2", "server_two"],
    ["Server 3", "server_three"],
    ["Status", "status"],
  ],

  user: [
    ["First Name", "firstName"],
    ["Last Name", "lastName"],
    ["Sex", "sex"],
    ["Phone Number", "phone"],
  ]
}
