import React from 'react'
import { Col, Row, Divider, Table } from 'antd';
import { useNavigate, Link, useOutletContext } from 'react-router-dom';

const columns= [
  {
    title: 'Full Name',
    width: 100,
    dataIndex: 'name',
    key: 'name',
    fixed: 'left',
  },
  {
    title: 'Age',
    width: 100,
    dataIndex: 'age',
    key: 'age',
    fixed: 'left',
  },
  {
    title: 'Column 1',
    dataIndex: 'address',
    key: '1',
    width: 150,
  },
  {
    title: 'Column 2',
    dataIndex: 'address',
    key: '2',
    width: 150,
  },
  {
    title: 'Column 3',
    dataIndex: 'address',
    key: '3',
    width: 150,
  },
  {
    title: 'Column 6',
    dataIndex: 'address',
    key: '6',
    width: 150,
  },
  {
    title: 'Column 7',
    dataIndex: 'address',
    key: '7',
    width: 300,
  },
  { title: 'Column 8', dataIndex: 'address', key: '8' },
  {
    title: 'Action',
    key: 'operation',
    fixed: 'right',
    width: 100,
    render: () => <Link to="detail">Action</Link>,
  },
];

const data= [];
for (let i = 0; i < 50; i++) {
  data.push({
    key: i,
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`,
  });
}
const DetailAdmin = () => {
  const [pathname]= useOutletContext();
  return (
    <>
          <Row span={24} style={{background: "#fff", width: "100%", height: "50px", borderRadius: "5px"}} align='middle'>
            <Divider style={{margin: "0", fontSize:"2rem"}} orientation="left">Category: PHIM HÀNH ĐỘNG</Divider>
          </Row>

          <Row span={24} style={{background: "#fff", height: "auto", width: "100%", borderRadius: "5px", padding: "12px", marginTop: "24px"}}>
            <Row  gutter={16} style={{width: "100%", height: "80px", padding: "0"}}>
              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>

              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>
              <Col span={6} >ABC</Col>
            </Row>
            
            <Row >
              <Divider style={{margin: "0", fontSize:"2rem"}} orientation="left">Movie List</Divider>
              <Table columns={columns} dataSource={data} scroll={{ x: 1300, y: 200 }} size="large" bordered pagination={{pageSize: 5, style: {margin :"26px 0 0 0"}}}/>
            </Row>
          </Row>
    </>
  )
}

export default DetailAdmin