import React from 'react'
import { Col, Row, Divider, Table, Form, Input, Button } from 'antd';
import { useNavigate, Link, useOutletContext } from 'react-router-dom';

const data= [
  {
    id: 1,
    title: "ABC",
    slug:"aaa"
  }
];

const DetailAdmin = (aa) => {
  const [pathname]= useOutletContext();
  const [form] = Form.useForm();
  const navigate = useNavigate();

  return (
    <>
      <Row span={24} style={{background: "#fff", width: "100%", borderRadius: "5px"}} justify="space-around" align='middle'>
            <Col>
              <Divider style={{margin: "0", fontSize:"2rem"}} orientation="left">{pathname.charAt(0).toUpperCase() + pathname.slice(1)}</Divider>
            </Col>
            
            <Col>
              <Form
                form={form}
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                autoComplete="off"
                layout="inline"
              >

                  <Form.Item
                    label="Skip"
                    name="skip"
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Take"
                    name="take"
                  >
                    <Input />
                  </Form.Item>
                  
                  <Button
                    onClick={() => {
                      form.resetFields();
                    }}
                  >
                    Clear
                  </Button>

                  <Button type="primary" htmlType="submit" style={{ margin: '0 8px' }}>
                    Search
                  </Button>

                  <Button type="primary" htmlType="submit" onClick={()=> navigate("new")}	>
                    New
                  </Button>
              </Form>
            </Col>
          </Row>
          <Row className="gutter-row" span={24} style={{background: "#fff", height: "470px", borderRadius: "5px", padding: "12px", marginTop: "24px"}}>
            <Table columns={HeaderCol[pathname]} dataSource={data} scroll={{ x: 1300, y: 345 }}/>
          </Row>
    </>
  )
}

export default DetailAdmin

const HeaderCol= {
  category: [
    { title: "Id",
      key: "id",
      dataIndex: 'id',
      width: 30
    },
    {
      title: "Title",
      key: "title",
      dataIndex: 'title',
      width: 150,
    },
    { title: "Slug",
      key: "slug",
      dataIndex: 'slug',
      width: 80
    },

    { title: "Status",
      key: "status",
      dataIndex: 'status',
      width: 100
    },

    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: () => (
      <>
        <Button type="primary">Update</Button>
        <Link to="detail" ><Button style={{marginLeft: "10px"}}>Detail</Button></Link>
        <Button type="danger" style={{marginLeft: "10px"}}>Delete</Button>
      </>
      )
    },
  ],

  movie: [
    { title: "Id",
      key: "id",
      dataIndex: 'id',
      width: 30
    },
    {
      title: "Title",
      key: "title",
      dataIndex: 'title',
      width: 150,
    },
    { title: "National",
      key: "national",
      dataIndex: 'national',
      width: 80
    },

    { title: "Type",
      key: "type",
      dataIndex: 'type',
      width: 100
    },

    { title: "Duration",
      key: "time_movie",
      dataIndex: 'time_movie',
      width: 100
    },

    { title: "Status",
      key: "status",
      dataIndex: 'status',
      width: 100
    },

    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: () => (
      <>
        <Button type="primary">Update</Button>
        <Link to="detail" ><Button style={{marginLeft: "10px"}}>Detail</Button></Link>
        <Button type="danger" style={{marginLeft: "10px"}}>Delete</Button>
      </>
      )
    },
  ],

  episode: [
    { title: "Id",
      key: "id",
      dataIndex: 'id',
      width: 30
    },
    {
      title: "Title",
      key: "episode_title",
      dataIndex: 'episode_title',
      width: 150,
    },
    { title: "Server 1",
      key: "server_one",
      dataIndex: 'server_one',
      width: 80
    },

    { title: "Server 2",
      key: "server_two",
      dataIndex: 'server_two',
      width: 100
    },

    { title: "Server 3",
      key: "server_three",
      dataIndex: 'server_three',
      width: 100
    },

    { title: "Status",
      key: "status",
      dataIndex: 'status',
      width: 100
    },

    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: () => (
      <>
        <Button type="primary">Update</Button>
        <Link to="detail" ><Button style={{marginLeft: "10px"}}>Detail</Button></Link>
        <Button type="danger" style={{marginLeft: "10px"}}>Delete</Button>
      </>
      )
    },
  ],

  user: [
    { title: "Id",
      key: "id",
      dataIndex: 'id',
      width: 30
    },
    {
      title: "Last Name",
      key: "lastName",
      dataIndex: 'lastName',
      width: 150,
    },
    { title: "Frist Name",
      key: "firstName",
      dataIndex: 'firstName',
      width: 80
    },

    { title: "Sex",
      key: "sex",
      dataIndex: 'sex',
      width: 100
    },

    { title: "Phone number",
      key: "phone",
      dataIndex: 'phone',
      width: 100
    },

    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: () => (
      <>
        <Button type="primary">Update</Button>
        <Link to="detail" ><Button style={{marginLeft: "10px"}}>Detail</Button></Link>
        <Button type="danger" style={{marginLeft: "10px"}}>Delete</Button>
      </>
      )
    },
  ],
}