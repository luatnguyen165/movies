import React from 'react'
import OverallData from './OverallData';
import "../styles/overalldisplay.scss"
import { Tabs } from 'antd';

const OverallDisplay = () => {
  return (
    <div className='overallDisplay-wrapper'>
         <Tabs
            
            className='overallDisplay-tabs'
            defaultActiveKey="1"
            style={{width: "1375px"}}
            items={[
            {
                label: `Sales`,
                key: '1',
                children: <OverallData />,
            },
            {
                label: `Views`,
                key: '2',
                children: <OverallData />,
            },
            {
                label: `Movies`,
                key: '3',
                children: <OverallData />,
            },
            ]}
        />
    </div>
  )
}

export default OverallDisplay