
import { Routes, Route } from "react-router-dom"
import './App.scss';
import Admin from "./adminPage/components/Admin";
import Dashboard from "./adminPage/pages/Dashboard";
import Main from "./components/Main";
import ListAdmin from "./adminPage/components/ListAdmin"
import Home from './pages/Home';
import Watch from "./pages/Watch";
import User from "./adminPage/pages/User";
import Episode from "./adminPage/pages/Episode";
import MovieAdmin from "./adminPage/pages/Movie";
import Category from "./adminPage/pages/Category"
import New from "./adminPage/components/New";
import DetailAdmin from "./adminPage/components/DetailAdmin";
import Detail from "./pages/Detail"

function App() {
  return (
    <div className = "app-wrapper">
      
      <Routes>
        <Route path = "/" >
          <Route element= {<Home />} >
            <Route index element={<Main />}/>

            <Route path= "watch" element= {<Watch />} />

            <Route path= "detail" element= {<Detail />} />
          </Route>
          
          <Route path="admin" element={<Admin/>}>
            <Route index element={<Dashboard />}/>

            <Route path= "category" element= {<Category />} >

              <Route index element= {<ListAdmin />} />

              <Route path="detail" element= {<DetailAdmin />} />

              <Route path="new" element= {<New />} />
            </ Route>

            <Route path= "movie" element= {<MovieAdmin />} >
              <Route index element= {<ListAdmin />} />

              <Route path="detail" element= {<DetailAdmin />} />

              <Route path="new" element= {<New />} />
            </Route>

            <Route path= "episode" element= {<Episode />} >
              <Route index element= {<ListAdmin />} />

              <Route path="detail" element= {<DetailAdmin />} />

              <Route path="new" element= {<New />} />
            </Route>

            <Route path= "user" element= {<User />} >
              <Route index element= {<ListAdmin />} />

              <Route path="detail" element= {<DetailAdmin />} />

              <Route path="new" element= {<New />} />
            </Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
